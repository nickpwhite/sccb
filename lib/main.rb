# frozen_string_literal: true

require "csv"
require "parser"

class Main
  def initialize(output, args = [])
    @output = output

    @options = Parser.parse(output, args)
  end

  def run
    add_new_entries
    output_totals
  end

  def combine_entries(new_entries, base_entries)
    output_filename = options.output_filename || options.base_filename

    CSV.open(output_filename, "w+") do |csv|
      csv << base_entries.headers
      new_entries.each { |e| csv << e unless base_entries.include? e }
      base_entries.each { |e| csv << e }
    end
  end

  def output_totals
    n_total, m_total = total
    output.puts "Nick owes: #{n_total}"
    output.puts "Mich owes: #{m_total}"
  end

  def total
    output_filename = options.output_filename || options.base_filename
    who_rows = rows_per_payer(output_filename)

    both_debits, both_credits = get_totals(who_rows[nil])
    n_debits, n_credits = get_totals(who_rows["N"])
    m_debits, m_credits = get_totals(who_rows["M"])

    n_total = (n_debits + (both_debits / 2)) - (n_credits + (both_credits / 2))
    m_total = (m_debits + (both_debits / 2)) - (m_credits + (both_credits / 2))

    [n_total.round(2), m_total.round(2)]
  end

  private

  def add_new_entries
    return if options.new_filename.nil? || options.new_filename == ""

    new_entries = CSV.read(options.new_filename, headers: true)
    new_entries.each do |entry|
      entry.each { |header, value| output.puts "#{header}: #{value}" }
      output.print "Who made this transaction (leave blank for shared) > "
      who = gets.strip.capitalize
      who = nil if who == ""
      entry << ["Who", who]
    end

    combine_entries(new_entries, existing_transactions)
  end

  def existing_transactions
    @existing_transactions ||= CSV.read(options.base_filename, headers: true)
  end

  def rows_per_payer(filename)
    who_rows = Hash.new { |h, k| h[k] = [] }

    CSV.open(filename, headers: true) do |csv|
      csv.each { |row| who_rows[row["Who"]] << row }
    end

    who_rows
  end

  def get_totals(rows)
    debits = credits = 0.0
    rows.each do |row|
      debits += (row["Debit"]&.gsub(/\$/, "") || 0).to_f
      credits += (row["Credit"]&.gsub(/\$/, "") || 0).to_f
    end

    [debits, credits]
  end

  attr_reader :output
  attr_accessor :options
end
