# frozen_string_literal: true

class Options
  attr_reader :base_filename, :new_filename, :output_filename

  def initialize(output = $stdout)
    @base_filename = nil
    @new_filename = nil
    @output_filename = nil
    @output = output
  end

  def define_options(parser)
    parser.banner = "Usage: sccb command [options]"
    parser.separator ""
    parser.separator "Options:"

    parser.on(
      "-bFILE.csv", "--base=FILE.csv", "Use FILE.csv as a base list of transactions"
    ) { |file| self.base_filename = file }

    parser.on(
      "-nFILE.csv", "--new=FILE.csv", "Use FILE.csv as new transactions to add"
    ) { |file| self.new_filename = file }

    parser.on(
      "-oFILE.csv", "--output=FILE.csv",
      "Use FILE.csv as the output file, this can be omitted to use the base file"
    ) { |file| self.output_filename = file }

    parser.separator ""
    parser.separator "Common Options:"

    parser.on_tail("-h", "--help", "Print this help") do
      output.puts parser
      exit
    end
  end

  private

  attr_accessor :output
  attr_writer :base_filename, :new_filename, :output_filename
end
