# frozen_string_literal: true

require "options"
require "optparse"

class Parser
  def self.parse(output, args)
    options = Options.new(output)

    OptionParser.new do |parser|
      options.define_options(parser)

      parser.parse!(args)
    end

    options
  end
end
