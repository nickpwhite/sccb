# frozen_string_literal: true

require "main"

RSpec.describe(Main) do
  let(:output) { StringIO.new }

  context "when not given a base filename" do
    it "prints the default filename to the given output" do
      main = described_class.new(output)

      main.run

      expect(output.string.strip).to eq("base.csv")
    end
  end

  context "when given a base filename" do
    it "prints the given filename to the given output" do
      filename = "test.csv"
      main = described_class.new(output, ["-b#{filename}"])

      main.run

      expect(output.string.strip).to eq(filename)
    end
  end
end
